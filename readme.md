#Prototype

Welcome by project Prototype

###Table of Contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

###General Info
This wpf application is made for a scrum team to do scrum. In this application team members can work together 
on a project, they can add sprints, backlogitems and they can see their progress on the scrum board.
Every team member can make an own account.

###Technologies
To work on the project, the following things are required:
- Visual Studio 2019
- Windows 10

###Set up
To start working on the project you need to clone it (`git clone`).
After cloning, open the project in Visual Studio and download the NuGet packages and dependecies.

To use the installer, follow the following steps:
- Select a solution, click on 'Build' and then on 'Batch build'
- Select 'release'
- Go to Project folder and in the folder the click on .msi installer
- Then choose the right directory

####Note 
When you start the project for the first time, you should start it only once!

